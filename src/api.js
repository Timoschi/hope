import {
        initializeApp
} from 'firebase/app';
import {
        getFirestore,
        setDoc,
        doc,
        onSnapshot,
        collection,
        getDocs
} from "firebase/firestore";
import {
        getAuth,
        signInWithEmailAndPassword
} from "firebase/auth";
import {
        getStorage,
        ref,
        getDownloadURL
} from "firebase/storage";
import {
        getAnalytics
} from "firebase/analytics";

const firebaseConfig = {
        apiKey: "AIzaSyCU5V0mqxqchUKiWTdY5q9piAgg77nmb7k",
        authDomain: "hope-3a0c8.firebaseapp.com",
        projectId: "hope-3a0c8",
        storageBucket: "hope-3a0c8.appspot.com",
        messagingSenderId: "738857486909",
        appId: "1:738857486909:web:c35a0a43ed594f21ddb854",
        databaseURL: "https://hope-3a0c8-default-rtdb.europe-west1.firebasedatabase.app/",
        measurementId: "G-LDPT5G8TPY"
};



const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth();
const storage = getStorage(app);
// eslint-disable-next-line no-unused-vars
const analytics = getAnalytics(app);

function getImage(period, number, cb) {
        getDownloadURL(ref(storage, `highResImages/${period}/l_thumb_${number}.jpg`)).then(url => {
                cb(url);
        })
}

async function setCollected(amount) {
        const isConfirmed = confirm("Jste si jistí?");
        if (isConfirmed) {
                if (amount > 0) {
                        await setDoc(doc(db, "persistence", "collected"), {
                                collected: amount
                        });
                        alert("Hodnota uložena!");
                } else {
                        alert(
                                "Zkontrolujte, že pole není prázdné a že je zadaná hodnota alespoň 1 Kč"
                        );
                }
        } else {
                alert("error with amount change")
        }
}

function getCollected(cb) {
        onSnapshot(doc(db, "persistence", "collected"), (doc) => {
                cb(doc.data().collected)
        });

}

function getArticles(cb) {
        onSnapshot(collection(db, "news"), (docs) => {
                let result = [];
                docs.forEach(e => result.push(e.data()));
                cb(result);
        });
}

async function getArticlesLength(cb) {
        const snap = await getDocs(collection(db, "news"));
        let len = 0;
        snap.forEach(() => len++);
        cb(len)
}

function login(email, password, succF, errF) {
        signInWithEmailAndPassword(auth, String(email), String(password)).then(
                userCredential => {
                        succF(userCredential.user);
                }
        ).catch(error => {
                errF(error.message);
        });
}

async function saveArticle(articleID, articleObj) {
        await setDoc(doc(db, "news", articleID), articleObj);
}

//testing purposes

let defaultDoc = {
        date: "1.1.2021",
        text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint aliquam iste excepturi neque magni sequi alias rem impedit nobis in dicta doloremque ad deserunt corporis vel, fugit, voluptatibus nemo repellat natus sunt quos. Commodi, placeat. Iusto, quia nam, dolor numquam odit unde quod soluta molestiae maiores perspiciatis nesciunt animi optio.",
        imagesData: ["2-min.jpeg", "2-min.jpeg", "2-min.jpeg"],
        imageAlts: ["imageAlt1", "imageAlt2", "imageAlt3"]
}
defaultDoc;

export default {
        setCollected,
        getCollected,
        login,
        getArticles,
        getImage,
        saveArticle,
        getArticlesLength
};